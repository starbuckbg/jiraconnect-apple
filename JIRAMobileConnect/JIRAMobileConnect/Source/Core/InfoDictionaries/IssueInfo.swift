//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension Issue {
  var infoAsDictionary: [String: NSObject] {
    return ["summary": summary,
            "description": description,
            "components": components,
            "type": type]
  }
}
