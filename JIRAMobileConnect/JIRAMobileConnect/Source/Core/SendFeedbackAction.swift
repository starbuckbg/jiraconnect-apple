//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

public class SendFeedbackAction: AsyncAction {
  let issue: Issue
  let screenshotImageOrNil: UIImage?
  let onComplete: Outcome<Void>->Void
  
  var getTargetAction: GetJMCTargeAction!
  var postFeedbackAction: HTTPPostFeedbackAction!
  
  public init(issue: Issue, screenshotImageOrNil: UIImage? = nil, onComplete: Outcome<Void>->Void) {
    self.issue = issue
    self.screenshotImageOrNil = screenshotImageOrNil
    self.onComplete = onComplete
    
    super.init()
  }
  
  override public func run() {
    getTargetAction = GetJMCTargeAction { outcome in
      switch outcome {
      case .Success(let JMCTarget):
        self.postFeedbackToTarget(JMCTarget)
      case .Error(let error):
        self.finishedExecutingOperationWithOutcome(.Error(error))
      case .Cancelled:
        break
      }
    }
    getTargetAction.start()
  }
  
  func postFeedbackToTarget(target: JMCTarget) {
    postFeedbackAction = HTTPPostFeedbackAction(issue: issue, screenshotImageOrNil: screenshotImageOrNil, target: target) { outcome in
     self.finishedExecutingOperationWithOutcome(outcome)
    }
    postFeedbackAction.start()
  }
  
  func finishedExecutingOperationWithOutcome(outcome: Outcome<Void>) {
    finishedExecutingOperation()
    onComplete(outcome)
  }
}
