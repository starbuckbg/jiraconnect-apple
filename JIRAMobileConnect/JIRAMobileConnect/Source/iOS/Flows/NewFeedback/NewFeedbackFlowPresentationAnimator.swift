//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class NewFeedbackFlowPresentationAnimator: ViewControllerTransitioningAnimator {
  // View Controllers
  private var newFeedbackFlowController: NewFeedbackFlowController!
  // Views
  private var feedbackView: NewFeedbackView!
  private var animatedScreenshotImageView: UIView!
  private var animatedDimmingView: UIView!
  
  override func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
    return 0.5 // This animator is driven by keyboard presentation, this duration is an arbitrary value.
  }
  
  override func animateTransition() {
    participants.swapViewControllerViews() // Next step could throw, this line needs to run in any case.
    do {
      try assignProperties()
      setUpTransition()
    } catch {
      assertionFailure("\(error)".formattedLoggingStatement)
      animateTransitionContext.completeTransition(false)
      return
    }
  }
  
  func assignProperties() throws {
    let newFeedbackFlowTransitioningParticipants = try NewFeedbackFlowTransitioningParticipants(viewControllerTransitioningParticipants: participants)
    newFeedbackFlowController = newFeedbackFlowTransitioningParticipants.newFeedbackFlowController
    feedbackView = newFeedbackFlowTransitioningParticipants.newFeedbackView
    let screenshotImage = newFeedbackFlowTransitioningParticipants.newFeedbackViewController.screenshotImage
    animatedScreenshotImageView = createAnimatedScreenshotTransitionContainerViewWithScreenshot(screenshotImage!) // TODO: Guard that screenshot image is not nil.
    
    animatedDimmingView = createAnimatedDimmingView()
  }
  
  func createAnimatedScreenshotTransitionContainerViewWithScreenshot(screenshotImage: UIImage) -> UIView {
    let imageView = UIImageView(image: screenshotImage)
    imageView.layer.cornerRadius = 8
    
    let containerView = HuggingContainerView()
    containerView.backgroundColor = UIColor.clearColor()
    containerView.clipsToBounds = true
    containerView.viewToHug = imageView // This adds adds image view as subview.
    return containerView
  }
  
  func createAnimatedDimmingView() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.blackColor()
    view.alpha = 0
    view.clipsToBounds = true
    return view
  }
  
  func setUpTransition() {
    startObservingKeyboardPresentation()
    
    setUpAnimatedViewsWithInitialFrame(participants.transitionContainerView.bounds)
    newFeedbackFlowController.navigationBarHidden = true
    setUpFeedbackView()
    feedbackView.presentKeyboard()
  }
  
  func setUpAnimatedViewsWithInitialFrame(initialFrame: CGRect) {
    animatedScreenshotImageView.frame = initialFrame
    animatedScreenshotImageView.layer.cornerRadius = 0
    
    animatedDimmingView.frame = initialFrame
    animatedDimmingView.layer.cornerRadius = 0
  }
  
  func setUpFeedbackView() {
    feedbackView.screenshotPreviewView.deleteScreenshotButton?.alpha = 0
    feedbackView.hideScreenshot()
    feedbackView.insertSubviewBelowComposeFeedbackView(subview: animatedScreenshotImageView)
    feedbackView.insertSubview(animatedDimmingView, aboveSubview: animatedScreenshotImageView)
  }
  
  func handleKeyboardPresentationNotification(notification: NSNotification) {
    guard let userInfo = notification.userInfo else { return }
    
    let keyboardAnimationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
    let animationCurve = UIViewAnimationOptions(rawValue: (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.unsignedIntegerValue ?? UInt(0))
    animate(keyboardAnimationDuration, options: [animationCurve])
  }
  
  func animate(duration: NSTimeInterval, options: UIViewAnimationOptions) {
    var finalFrameForAnimatedScreenshotImageView = feedbackView.uncroppedScreenshotImageViewFrame()
    finalFrameForAnimatedScreenshotImageView.origin.y -= newFeedbackFlowController.topLayoutGuide.length
    
    UIView.animateWithDuration(duration,
      delay: 0,
      options: options,
      animations: {
        self.animatedScreenshotImageView.frame = finalFrameForAnimatedScreenshotImageView
        self.newFeedbackFlowController.navigationBarHidden = false
        self.animatedScreenshotImageView.layer.cornerRadius = 8
        
        self.animatedDimmingView.frame = finalFrameForAnimatedScreenshotImageView
        self.animatedDimmingView.layer.cornerRadius = 8
        self.animatedDimmingView.alpha = self.feedbackView.dimmingViewAlpha
      },
      completion: { (completed) -> Void in
        self.animatedScreenshotImageView.removeFromSuperview()
        self.animatedDimmingView.removeFromSuperview()
        
        self.feedbackView.showScreenshot()
        self.stopObservingNotifications()



        self.animateTransitionContext.completeTransition(true)
    })
  }
  
  func startObservingKeyboardPresentation() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewFeedbackFlowPresentationAnimator.handleKeyboardPresentationNotification(_:)), name: UIKeyboardWillShowNotification, object: nil)
  }
  
  func stopObservingNotifications() {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  deinit {
    stopObservingNotifications()
  }
}

class NewFeedbackFlowTransitioningParticipants {
  private(set) var newFeedbackFlowController: NewFeedbackFlowController!
  private(set) var newFeedbackViewController: NewFeedbackViewController!
  private(set) var newFeedbackView: NewFeedbackView!
  
  init(viewControllerTransitioningParticipants tp: ViewControllerTransitioningParticipants) throws {
    guard let newFeedbackFlowController = tp.toViewController as? NewFeedbackFlowController,
      let newFeedbackViewController = newFeedbackFlowController.viewControllers[0] as? NewFeedbackViewController,
      let newFeedbackView = newFeedbackViewController.view as? NewFeedbackView
      else {
        throw NewFeedbackFlowTransitioningError.ExtractParticipantsError
    }
    self.newFeedbackFlowController = newFeedbackFlowController
    self.newFeedbackViewController = newFeedbackViewController
    self.newFeedbackView = newFeedbackView
  }
}

enum NewFeedbackFlowTransitioningError: String, ErrorType {
  case ExtractParticipantsError = "newFeedbackFlowController does not contain a child ComposeFeedbackViewController at index 0."
}
