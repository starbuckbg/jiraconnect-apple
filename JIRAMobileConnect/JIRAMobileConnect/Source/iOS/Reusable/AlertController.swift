//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class AlertController: UIAlertController {
  var lastPresentingViewController: UIViewController?
  var useCustomTransition: Bool = true
  var feedbackSettings: FeedbackSettings? = nil
  
  override func viewWillAppear(animated: Bool) {
    if let viewController = presentingViewController {
      lastPresentingViewController = viewController
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.tintColor = ADGTintColor
  }
}
